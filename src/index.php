<?
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: X-Requested-With");

function getWalkScore($lat, $lon, $address) {
  $WALKSCORE_API_KEY = getenv('WALKSCORE_API_KEY');

  $address=urlencode($address);
  $url = "http://api.walkscore.com/score?format=json&address=$address";
  $url .= "&lat=$lat&lon=$lon&wsapikey=$WALKSCORE_API_KEY";
  $str = @file_get_contents($url);

  return $str;
 }

 $lat = $_GET['lat'];
 $lon = $_GET['lon'];
 $address = stripslashes($_GET['address']);
 $json = getWalkScore($lat,$lon,$address);

 echo $json;
?>
