# Walkscore Heroku Service

## Required

- Heroku
- Walkscore API key

```sh
# First time
heroku login

# Create project
heroku create

# Add API key to heroku env
heroku config:set WALKSCORE_API_KEY=<API_KEY>

# Publish project
git push heroku master
```

Heroku will return a URL for you to use.

<URl>?lat=xxx&lon=xxx&address=xxx

eg https://murmuring-brushlands-97540.herokuapp.com/?address=1119%8th%20Avenue%20Seattle%20WA%2098101&lat=47.6085&lon=-122.3295

## References

<https://www.walkscore.com/professional/api.php>
